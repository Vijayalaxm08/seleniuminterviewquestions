Abstract Class:

	An abstract class can not be instantiated.

Syntax:
	abstract class Shape{
	// code
	}

Abstract Methods:
	
	An Abstract Method is a method that has just the method definition but does not contain implementation.

Syntax:
	abstract public void calculateArea();
	
	For an abstract method, no implementation is required. Only the signature of the method is defined.
	
Abstract Class in Java:

	An abstract class may also have concrete (complete) methods.
	For design purpose, a class can be declared abstract even if it does not contain any abstract methods
	Reference of an abstract class can point to objects of its sub-classes thereby achieving run-time polymorphism
	Ex: Shape obj = new Rectangle();
	A class must be compulsorily labeled abstract, if it has one or more abstract methods.
	
Final Keyword in Java:
	
	The final modifier applies to classes, methods, and variables. The meaning of final varies from context t0
	 context, but the essential idea is the same.
	
	A final class may not be inherited
	A final variable becomes a constant and its value can not be changed.
	A final method may not be overridden. This is done for security reasons, and these methods are used for
	optimization.
	A final class can not be inherited