
Frames:
	
	An iFrame (Inline Frame) is an HTML document embedded inside the current HTML document on a website.
	iFrame HTML element is used to insert content from another source, such as an advertisement,
	into a Web page. A Web designer can change an iFrame's content without making them reload the
	complete website. A website can have multiple frames on a single page. And a frame can also have inner
	frames (Frame in side a Frame)


	*Index of the iframe starts with '0'.

Frame: -> WebDriver Interface ->TargetLocator Interface

Implementing Class:
	1. EventFiringTargetLocator
	2. RemoteTargetLocator
	
3 Ways to Switch to Frames:
	1. By Id or Name
		driver.switchTo().frame("nameorid");
	2. By Index
	
		driver.switchTo().frame(0);
		driver.switchTo().defaultContent();
		
	3. By WebElement
		WebElement frame1=driver.findElementById("");
		driver.switchTo().frame(frame1);

Find Total Number of Frames in a Page:

	int size = driver.findElements(By.tagName("iframe")).size();
	System.out.println(size);

Exceptions:
	NoSuchFrameException

	
