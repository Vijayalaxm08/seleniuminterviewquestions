package frames;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrames {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/frame.html");
		
		driver.manage().window().maximize();
		
		driver.switchTo().frame(0);
		driver.findElementByXPath("//button[text()='Click Me']").click();
		String string = driver.findElementById("Click").getText();
		System.out.println(string);
		driver.switchTo().defaultContent();
		driver.close();
		
		//Nested Frame
		driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/frame.html");
		driver.manage().window().maximize();
		driver.switchTo().frame(1);
		driver.switchTo().frame("frame2");
		driver.findElementByXPath("//button[text()='Click Me']").click();
		driver.switchTo().defaultContent();
		
		//Total Number of Frames in a Page
		
		int size = driver.findElements(By.tagName("iframe")).size();
		System.out.println(size);
		
		
		
	}

}
