WAITS IN SELENIUM:

import java.util.concurrent.TimeUnit;

Implicit Wait:
	Implicit waits will be in place for the entire time the browser is open. 
	Implicit waits are used to provide a default waiting time (say 30 seconds) between each
	consecutive test step/command across the entire test script. Thus, the subsequent test step would
	only execute when the 30 seconds have elapsed after executing the previous test step/command.
	
Syntax:
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
Fluent Wait:
	The fluent wait is used to tell the web driver to wait for a condition, as well as the
	frequency with which we want to check the condition before throwing an "ElementNotVisibleException"
	exception.
Frequency: Setting up a repeat cycle with the time frame to verify/check the condition at the regular 
		   interval of time

Syntax:
	Wait wait = new FluentWait(driver)
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);

Explicit Wait:
	The explicit wait is used to tell the Web Driver to wait for certain conditions (Expected Conditions)
	or the maximum time exceeded before throwing an "ElementNotVisibleException" exception.
	
Syntax:
	WebDriverWait wait=new WebDriverWait(driver, 20);
	WebElement abc;
	abc= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='useer']")));
  	abc.click();

The following are the Expected Conditions that can be used in Explicit Wait

 1. alertIsPresent()
 2. elementSelectionStateToBe()
 3. elementToBeClickable()
 4. elementToBeSelected()
 5. frameToBeAvaliableAndSwitchToIt()
 6. invisibilityOfTheElementLocated()
 7. invisibilityOfElementWithText()
 8. presenceOfAllElementsLocatedBy()
 9. presenceOfElementLocated()
10. textToBePresentInElement()
11. textToBePresentInElementLocated()
12. textToBePresentInElementValue()
13. titleIs()
14. titleContains()
15. visibilityOf()
16. visibilityOfAllElements()
17. visibilityOfAllElementsLocatedBy()
18. visibilityOfElementLocated() 

PageLoadTimeout Command:
	Sets the amount of time to wait for a page load to complete before throwing an error.
	If the timeout is negative, page loads can be indefinite.

Syntax:
	driver.manage().timeouts().pageLoadTimeout(100, SECONDS);

SetScriptTimeout Command:
	Sets the amount of time to wait for an asynchronous script to finish execution before
	throwing an error. If the timeout is negative, then the script will be allowed to run indefinitely.

Syntax:
	 driver.manage().timeouts().setScriptTimeout(100,SECONDS);

Sleep Command:
	This is rarely used, as it always force the browser to wait for a specific time.
	Thread.Sleep is never a good idea and that�s why Selenium provides wait primitives.

Syntax:
	thread.sleep(1000);	
 	
 	
 	
 	