Extent Reports:
	Extent Reports is a customizable HTML report which can be integrated into 
	Selenium WebDriver using JUnit and TestNG frameworks.

Advantages of Extent Reports:
	1. Customisable HTML report with stepwise and pie chart representation.
	2. Displays the time taken for test case execution within the report.
	3. Each test step can be associated with a screenshot.
	4. Multiple test case runs within a single suite can be tracked easily.
	5. Can be easily integrated with TestNG and JUnit frameworks.
	
Classes Used:
	Reporter
	   |
    SeMethods
	   |
  ProjectMethods
       |
  TC001_CreateLead
  
@BeforeSuite - Project Methods - Reporter
	ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
	html.setAppendExisting(true);
	ExtentReports extent = new ExtentReports();
	extent.attachReporter(html);

@BeforeTest - TC001_CreateLead
public void setData() {
	testCaseName="TC001_CreateLead";
	testDescription="Creating a Lead";
	authors="Gopi";
	category="Smoke";
	testNodes ="Leads";
	dataSheetName="TC001_CreateLead";
	}

@BeforeClass - Project Methods - Reporter
	ExtentTest suiteTest = extent.createTest(testCaseName, testDescription);
  
@BeforeMethod - Project Methods - Reporter
	ExtentTest test = 	suiteTest.createNode(testNodes);
	test.assignCategory(category);
	test.assignAuthor(authors);
	startApp("chrome", "http://leaftaps.com/opentaps");	
  
@Test - TC001_CreateLead

@AfterMethod - Project Methods
	closeAllBrowsers();

@AfterTest - Nothing

@AfterSuite - Project Methods - Reporter
	extent.flush();
	
Reporter Step: Reporter - SeMethods
	MediaEntityModelProvider img = null;
	snapNumber = takeSnap();
	img = MediaEntityBuilder.createScreenCaptureFromPath
						("./../reports/images/"+snapNumber+".jpg").build();
	test.pass(desc, img);
	test.fail(desc, img);
	est.warning(desc, img);
	test.info(desc);
	
public long takeSnap(){
		long number = (long) Math.floor(Math.random() * 900000000L) + 10000000L; 
		try {
			FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE) , new File("./reports/images/"+number+".jpg"));
		} 
		return number;
	}



	