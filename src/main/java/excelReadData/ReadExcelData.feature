READ EXCEL DATA:

Steps:
1. Open Workbook
2.Go to Sheet
3. Go To row
4. Go to Column/Cell
5. Read data

ReadExcelData.java:
public static String[][] readExcelData(String fileName) throws IOException {
		
		//1. Open Workbook
		XSSFWorkbook wbook=new XSSFWorkbook("./data/"+fileName+".xlsx");
					
		//2. Go to Sheet
		XSSFSheet sheet=wbook.getSheetAt(0);
		
		//Row Count
		int rowCount=sheet.getLastRowNum();
		
		//Column Count
		int columnCount=sheet.getRow(0).getLastCellNum();
		
		String[][] data=new String[rowCount][columnCount];
		for (int i = 1; i <= rowCount; i++) {
			
			//3. Go To Row
			XSSFRow row=sheet.getRow(i);
			for (int j = 0; j < columnCount; j++) {
				
				//4. Go To Column
				XSSFCell column=row.getCell(j);
				
				//Read Data
				data[i-1][j]=column.getStringCellValue();
			}			
		}		
		return data;		
	}

TC001.java:
@DataProvider(name="fetchData")
	public String[][] getData() throws IOException {
		
		return ReadExcelData.readExcelData("Tc001_CreateLead");
		
	}
@Test(dataProvider="fetchData")
	public void createLead(String compName, String fName) throws InterruptedException, IOException {
	
	WebElement eleCompName = locateElement("id","createLeadForm_companyName");
	type(eleCompName,compName);
	WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
	type(eleFirstName,fName);
}

If we have DataProvider at another class

@Test(dataProviderClass=TC002_EditLead.class, dataProvider="EditData")
		