Listeners:
	
	Listener is defined as interface that modifes the default TestNG's behavior. As the name
	suggests Listeners "listen" to the event defined in the selenium script and behave accordingly.
	It is used in selenium by implementing Listeners Interface. It allows customizing TestNG reports or	logs.
	 	
There are two main listeners.
	1. WebDriver Listeners
	2. TestNG Listeners

TestNG Listeners:
The below are Interface
	IAnnotationTransformer ,
	ITestListener ...etc

ITestListener has following methods

	1. OnStart- OnStart method is called when any Test starts.
	2. onTestSuccess- onTestSuccess method is called on the success of any Test.
	3. onTestFailure- onTestFailure method is called on the failure of any Test.
	4. onTestSkipped- onTestSkipped method is called on skipped of any Test.
	5. onTestFailedButWithinSuccessPercentage- method is called each time Test fails but is within success percentage.
	6. onFinish- onFinish method is called after all Tests are executed.
	
Two Class:
	1. ListenerClass implements ITestListener - We add the above methods here
	2. TestCases class - We link out Listener Class to this class using Listener Annotations

Annotations:
@Listeners(Listener_Demo.ListenerClass.class) - packageName.ClassName.classKeyword 
declare before public class TestCases()

Use of Listener for multiple classes:
	If project has multiple classes adding Listeners to each one of them could be cumbersome and error prone.
	In such cases, we can create a testng.xml and add listeners tag in XML.

	<listeners>
	<listener class-name="Listener_Demo.ListenerClass"/>
	</listeners>

