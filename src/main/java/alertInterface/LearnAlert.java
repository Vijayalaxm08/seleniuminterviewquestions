package alertInterface;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class LearnAlert {

	public static void main(String[] args) throws HeadlessException, AWTException, IOException, InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/Alert.html");
		
		driver.manage().window().maximize();
		
		//1. Simple Alert
		driver.findElementByXPath("//button[text()='Alert Box']").click();
		String text = driver.switchTo().alert().getText();
		System.out.println(text);
		Thread.sleep(4000);
		takeSnap();
		driver.switchTo().alert().accept();
		
		//2. Confirmation Alert
		driver.findElementByXPath("//button[text()='Confirm Box']").click();
		String text1 = driver.switchTo().alert().getText();
		System.out.println(text1);
		driver.switchTo().alert().dismiss();
		
		//3. Prompt Alert
		driver.findElementByXPath("//button[text()='Prompt Box']").click();
		String text2 = driver.switchTo().alert().getText();
		System.out.println(text2);
		driver.switchTo().alert().sendKeys("Hello");
		driver.switchTo().alert().accept();
		
		//4 Learn Line Breaks in Alert
		driver.findElementByXPath("//button[text()='Line Breaks?']").click();
		String text3 = driver.switchTo().alert().getText();
		System.out.println(text3);
		driver.switchTo().alert().accept();
		
		//3. Prompt Alert
		driver.findElementByXPath("//button[text()='Prompt Box']").click();
		Alert alert = driver.switchTo().alert();
		String text4 = alert.getText();
		System.out.println(text4);
		alert.sendKeys("Hello");
		alert.accept();
					
	}
	
	@Test
	public static void takeSnap() throws HeadlessException, AWTException, IOException, InterruptedException{
		
		// take snap		
		BufferedImage image = new Robot().createScreenCapture
		(new Rectangle(
				Toolkit.getDefaultToolkit().getScreenSize()));
		
		ImageIO.write(image, "png", new File("./data/Alert.png"));
	
	}

}
