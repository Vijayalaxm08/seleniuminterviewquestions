
ALERT: It is an Interface

 * Alert is a small message box which displays on-screen notification to give the user some kind of
   information or ask for permission to perform certain kind of operation. It may be also used for
   warning purpose.

*  It is not a part of WebElement hence we cannot interact with it in Browser Level.

*  Alerts are written in JavaScript
	
		function functionName{
					
		}

Syntax:

	Alert alert=driver.switchTo().alert();
	

Three Types:
	1. Simple Alert
	
		String text = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();		
		
	2. Confirmation Alert
		
		String text = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();	
		driver.switchTo().alert().dismiss();
		
	3. Prompt Alert
		
		String text2 = driver.switchTo().alert().getText();
		driver.switchTo().alert().sendKeys("Hello");
		driver.switchTo().alert().accept();
		driver.switchTo().alert().dismiss();
		
Exceptions in Alert:
	1. UnhandledAlertException
	2. NoAlertPresentException
	
	
Capture Screenshot of the Page with Alert:
	* Robot Class

Syntax:
	// take snap		
	BufferedImage image = new Robot().createScreenCapture
	(new Rectangle(
			Toolkit.getDefaultToolkit().getScreenSize()));
		
	ImageIO.write(image, "png", new File("./data/Alert.png"));

Exceptions for Capturing Screenshot:
	1. HeadlessException
	2. AWTException
	3. IOException
	4. InterruptedException