Annotations in TestNG:

Annotation Parameter
1. priority: @Test(priority=0) - Can be assigned negative values also

2. invocationCount: @Test(invocationCount=2) - It will run for two times

3. threadPoolSize :@Test(invocationCount=5, threadPoolSize=2) - It will run for 5 times will 2 threads in parallel

4. invocationTimeOut :@Test(invocationCount=2, invocationTimeOut=5000) - Will expect my 2TC to be completed by
5000ms. If not, ThreadTimeOutException will occur.

5. timeOuts : @Test(timeOuts=5000)

6. dependsOnMethods: @Test(dependsOnMethods="EditLead") - Will run only if EditLead method passes, else it will skip.
If dependsOnMethods and priority are given, then dependsOnMethods overrides priority, so dOM will check.

7. alwaysRun: @Test(dependsOnMethod="EditLead", alwaysRun = true) - irrespective of failure in EditLead, this
will run

8. enabled: @Test(enabled=false) - it will not run and will skip it. 

9. expectedExceptions: @Test(expectedExceptions=RunTimeException.Class) - The method will not throw any 
exception because we are expecting an exception here

10. groups: @Test(groups={"Sanity","Smoke"})

11. dependsOnGroups: @Test(groups= "Sanity", dependsOnGroups="Smoke")


-------------------------------------------------------------------------------------------------------------

Annotation & Description
1.  @BeforeSuite
The annotated method will be run only once before all tests in this suite have run.

2. @AfterSuite
The annotated method will be run only once after all tests in this suite have run.

3. @BeforeClass
The annotated method will be run only once before the first test method in the current class is invoked.

4. @AfterClass
The annotated method will be run only once after all the test methods in the current class have run.

5. @BeforeTest
The annotated method will be run before any test method belonging to the classes inside the <test> tag is run.

6. @AfterTest
The annotated method will be run after all the test methods belonging to the classes inside the <test> tag
have run.

7. @BeforeGroups
The list of groups that this configuration method will run before. This method is guaranteed to run shortly
before the first test method that belongs to any of these groups is invoked.

8. @AfterGroups
The list of groups that this configuration method will run after. This method is guaranteed to run shortly
after the last test method that belongs to any of these groups is invoked.

9. @BeforeMethod
The annotated method will be run before each test method.

10. @AfterMethod
The annotated method will be run after each test method.

11. @DataProvider
Marks a method as supplying data for a test method. The annotated method must return an Object[ ][ ],
where each Object[ ] can be assigned the parameter list of the test method. The @Test method that wants
to receive data from this DataProvider needs to use a dataProvider name equals to the name of this annotation.

12. @Factory
Marks a method as a factory that returns objects that will be used by TestNG as Test classes. The method
must return Object[ ].

13. @Listeners
Defines listeners on a test class.

14. @Parameters
Describes how to pass parameters to a @Test method.

15. @Test
Marks a class or a method as a part of the test.


