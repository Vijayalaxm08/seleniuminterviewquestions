TestNG:
	1. TestNG is an automation testing framework in which NG stands for "Next Generation".
       TestNG is inspired from JUnit which uses the annotations (@).
	2. Generate the report in a proper format including a number of test cases runs, the number
	   of test cases passed, the number of test cases failed, and the number of test cases skipped.
	3. Multiple test cases can be grouped more easily by converting them into testng.xml file. In which
	   you can make priorities which test case should be executed first.
	4. The same test case can be executed multiple times without loops just by using keyword called 'invocation count.'
	5. Using testng, you can execute multiple test cases on multiple browsers, i.e., cross browser testing.
	6. If any of the test case fails while executing multiple test cases, you can execute that specific
	   test case separately.
	7. The static method log of Reporter class can be used to store logging information which presents
	   in or.testng.
	8. Parallel testing is possible

Refactoring Window:
	1. Location
	2. Suite Name
	3. Test Name
	4. Class Selection
	5. Parallel Mode
	6. Thread count

Basic testng.xml:

	<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd">
	
	<suite name="Suite" verbose="5" thread-count="2" parallel="classes"> - it willl run twice parallely
		
		<parameter name="url" value="http://leaftaps.com/opentaps"></parameter>
		<parameter name="username" value="DemoSalesManager"></parameter>
	    <parameter name="password" value="crmsfa"></parameter>
		<parameter name="browser" value="chrome"></parameter>
		
		<groups>
		  	<run>
		  		<include name="Smoke"></include>
		  		<include name="Sanity"></include>
		  		<include name="Reg"></include>
		  		<include name="Common"></include>
		  		<exclude name="Reg"></exclude>
		  		<include name="S.*"></include> - Will run smoke and sanity
		  	</run>
		</groups>
		
		<listeners>
			<listener class-name="Listener_Demo.ListenerClass"/>
		</listeners>
  
		<test thread-count="5" name="leads" parallel="classes">
			<classes>
				<class name="testcases.Tc001_CreateLead />
			</classes>
		</test> <!-- Test -->
	</suite> <!-- Suite -->

Implemented in Class: ProjectSpecificMethods

@Parameters({"browser", "url", "username", "password"})
@BeforeMethod(groups="Common")
public void login(String browser, String URL, String userName, String password) 

TC002_Edit
@Test(groups= "Sanity", dependsOnGroups="Smoke",alwaysRun=true)

Command Line Execution:
Library, Bin and xml- path
	java �cp  "C:\Users\User\Desktop\Guru99\TestProject\lib\*;
	  C:\Users\User\Desktop\Guru99\TestProject\bin" org.testng.TestNG testng.xml

Reporter:
	Reporter is a class present in TestNG. It provides 4 different methods to store log information they are:

	Reporter.log(String s);
	
	