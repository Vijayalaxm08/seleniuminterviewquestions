package occuranceCharacters;

import java.util.HashMap;

public class PrintOccuranceOfCharacter {

	public static void main(String[] args) {
		
		String text="welcome to automation";
		char[] charArray=text.toCharArray();
		
		HashMap<Character, Integer> map=new HashMap<Character, Integer>();
		
		for (char c : charArray) {
			
			if (map.containsKey(c)) {
				map.put(c, map.get(c)+1);
			}else {
				map.put(c, 1);
			}
		}
		System.out.println(map);
	}

}


// Output:
//   { =2, a=2, c=1, t=3, e=2, u=1, w=1, i=1, l=1, m=2, n=1, o=4}