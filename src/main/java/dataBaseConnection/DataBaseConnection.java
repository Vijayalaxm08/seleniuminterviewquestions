package dataBaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBaseConnection {

	public void dataBase(String data) throws ClassNotFoundException, SQLException {
		
		final String JDBC_DRIVER="com.mysql.jdbc.Driver";
		final String JDBC_URL="jdbc:mysql://...:3306/opentaps";
		
		//DataBase Credentials
		final String user="dbuser";
		final String pass="test@123";
		
		Connection conn=null;
		Statement stmt=null;
		
		//Register JDBC Driver
		Class.forName(JDBC_DRIVER);
		
		//Open a Connection
		conn=DriverManager.getConnection(JDBC_URL, user, pass);
		
		//Execute a Query
		stmt=conn.createStatement();
		String sql="SELECT FIRST_NAME, LAST_NAME FROM LEAFTAPS WHERE LEADID="+data;
		
		ResultSet executeQuery=stmt.executeQuery(sql);
		
		//Read Data
		while (executeQuery.next()) {
			String fname=executeQuery.getString("FIRST_NAME");
			String lname=executeQuery.getString("LAST_NAME");
			System.out.println("The First Name and Last Name is "+fname +lname);			
		}
		
		//Close Connection
		executeQuery.close();
		stmt.close();
		conn.close();
		
	}
	
}
