DataBase Connectivity:

Pre-requisite:                           	Steps:
	1. Server (IP)							1. DataBase Credentials	
	2. Driver								2. Register JDBC driver
	3. Schema Name							3. Open a Connection
	4. UserName								4. Execute a Query
	5. Password								5. Read Data
											6. Close Connection

DataBaseConnectivity.java:

		final String JDBC_DRIVER="com.mysql.jdbc.Driver";
		final String JDBC_URL="jdbc:mysql://...:3306/opentaps";
		
		//DataBase Credentials
		final String user="dbuser";
		final String pass="test@123";
		
		Connection conn=null;
		Statement stmt=null;
		
		//Register JDBC Driver  - ClassNotFoundException
		Class.forName(JDBC_DRIVER);
		
		//Open a Connection  - SQLException
		conn=DriverManager.getConnection(JDBC_URL, user, pass);
		
		//Execute a Query
		stmt=conn.createStatement();
		String sql="SELECT FIRST_NAME, LAST_NAME FROM LEAFTAPS WHERE LEADID="+data;
		
		ResultSet executeQuery=stmt.executeQuery(sql);
		
		//Read Data
		while (executeQuery.next()) {
			String fname=executeQuery.getString("FIRST_NAME");
			String lname=executeQuery.getString("LAST_NAME");
			System.out.println("The First Name and Last Name is "+fname +lname);			
		}
		
		//Close Connection
		executeQuery.close();
		stmt.close();
		conn.close();

CreateLead.java: 
	DataBaseConnectivity db=new DataBaseConnectivity();
	db.dataBase(leadID);		
