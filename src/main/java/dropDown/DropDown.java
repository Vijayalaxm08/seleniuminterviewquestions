package dropDown;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Dropdown.html");
		
		driver.manage().window().maximize();
		
		//Select By Index
		WebElement eledd1=driver.findElementById("dropdown1");
		Select sc=new Select(eledd1);
		sc.selectByIndex(1);
		
		//Select By Text
		WebElement eledd2 = driver.findElementByName("dropdown2");
		Select sc1=new Select(eledd2);
		sc1.selectByVisibleText("UFT/QTP");
		
		//Select By Value
		WebElement eledd3=driver.findElementById("dropdown3");
		Select sc3=new Select(eledd3);
		sc3.selectByValue("4");
		
		//Get DropDown Options
		WebElement eledd4 = driver.findElementByClassName("dropdown");
		Select sc4=new Select(eledd4);
		List<WebElement> allOptions = sc4.getOptions();
		System.out.println("Total Options in Drop down are:" +allOptions.size());
		for (WebElement eachOption : allOptions) {
			System.out.println(eachOption.getText());
		}
		
		//Select using sendKeys
		driver.findElementByXPath("//option[text()='You can also use sendKeys to select']").click();
		
		
		//Select Multiple
		WebElement eledd6 = driver.findElementByXPath("(//div[@class='example'])[6]");
		WebElement sel = driver.findElementByXPath("(//option[text()='Selenium'])[6]");
		WebElement app = driver.findElementByXPath("(//option[text()='Appium'])[6]");
		WebElement uft = driver.findElementByXPath("(//option[text()='UFT/QTP'])[6]");
		//Select sc6=new Select(eledd6);
		Actions builder=new Actions(driver);
		builder.moveToElement(eledd6).sendKeys(Keys.CONTROL).click(sel).click(uft).click(app).perform();
				
	}

}
