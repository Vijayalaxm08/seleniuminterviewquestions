package duplicates;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class StringDuplicates {

	public static void main(String[] args) {
		
		//Method 1 : Using 2 For loop and if condition
		String[] strNames= {"python", "java", "c", "javascript", "python"};
		
		for (int i = 0; i < strNames.length; i++) {
			for (int j = i+1; j < strNames.length; j++) {
				
				if (strNames[i].equals(strNames[j])) {
					System.out.println(strNames[i]);
				}
			}
		}
		
		//Method 2: Using HashSet
		String[] strNames1= {"python", "java", "c", "javascript", "python","java"};
		
		Set<String> set=new HashSet<String>();
		for(String eachName:strNames1) {
			//Set does not allow duplicates, hence if we trying to add a duplicate value, it will return false
			if (set.add(eachName)==false) {
				System.out.println(eachName);
			}
		}
		
		//Method 3: HashMap
		String[] strNames2= {"python", "java", "c", "javascript", "python","java"};
		Map<String, Integer> map=new HashMap<String,Integer>();
		
		for(String eachName:strNames2) {
			
			if(map.containsKey(eachName)) {
				map.put(eachName, map.get(eachName)+1);
			}else {
				map.put(eachName, 1);
			}
		}
		
		System.out.println(map);	
		
		Set<Entry<String, Integer>> entrySet=map.entrySet();
		for (Entry<String, Integer> entry : entrySet) {
			if(entry.getValue()>1){
				System.out.println(entry.getKey());
			}
		}
		
	}

}


/*Output:
	
	python
	python
	java
	{python=2, java=2, c=1, javascript=1}
	python
	java
*/