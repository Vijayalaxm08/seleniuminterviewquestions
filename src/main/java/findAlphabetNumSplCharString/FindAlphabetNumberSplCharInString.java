package findAlphabetNumSplCharString;

public class FindAlphabetNumberSplCharInString {

	public static void main(String[] args) {
		
		String str=" $$ Welcome to 1st Automation Interview $$ ";
		
		StringBuffer alphabet=new StringBuffer();
		StringBuffer num=new StringBuffer();
		StringBuffer splChar=new StringBuffer();
		
		for (int i = 0; i < str.length(); i++) {
			
			if (Character.isAlphabetic(str.charAt(i))) {
				alphabet.append(str.charAt(i));
			}
			else if(Character.isDigit(str.charAt(i))){
				num.append(str.charAt(i));
			}
			else
				splChar.append(str.charAt(i));
		}
		System.out.println(alphabet);
		System.out.println(num);
		System.out.println(splChar);
	}

}
