package swappingNumbers;

public class SwapNumbersWithThridVariable {

	public static void main(String[] args) {
		
		int num1=40;
		int num2=20;
		int temp;
		
		temp=num1;
		num1=num2;
		num2=temp;
		
		System.out.println(num1);
		System.out.println(num2);
	}

}
