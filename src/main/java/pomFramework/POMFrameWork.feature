POM: Page Object Model

	Page Object Model is a design pattern to create Object Repository for web UI elements.
	Under this model, for each web page in the application, there should be corresponding page class.
	This Page class will find the WebElements of that web page and also contains Page methods
	which perform operations on those WebElements.
	Name of these methods should be given as per the task they are performing, i.e., if a loader is
	waiting for the payment gateway to appear, POM method name can be waitForPaymentScreenDisplay().
	
Advantages of Page Object Model Framework:
	1. Code reusability � We could achieve code reusability by writing the code once and use it in different tests.
	2. Code maintainability � There is a clean separation between test code and page specific code such as 
	locators and layout which becomes very easy to maintain code. Code changes only on Page Object Classes when
	a UI change occurs. It enhances test maintenance and reduces code duplication.
	3. Object Repository � Each page will be defined as a java class. All the fields in the page will be defined
	in an interface as members. The class will then implement the interface.
	4. Readability � Improves readability due to clean separation between test code and page specific code  

Page Factory:

	Page Factory is an inbuilt Page Object Model concept for Selenium WebDriver but it is very optimized.
	
	3 Annotations for Locating Element:
		1. @FindBy()
		2. @FindByAll()
		3. @FindByS()

Hierarchy:

	Reporter
		|
	SeMethods
		|
	ProjectMethods
	/		\
  Pages    TestCases
  
  
Pages Class:
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="username")
	private WebElement eleUsername;
	public LoginPage typeUsername(String data) {
		type(eleUsername, data);
		return this;
	}
	//@FindBy(id="password")
	//private WebElement elePassword;
	public LoginPage typePassword(String data) {
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, data);
		return this;
	}
	@FindBy(className ="decorativeSubmit")
	private WebElement eleLogin;
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}
	
Page Object and Page Factory:
	
	Page Object is a class that represents a web page and hold the functionality and members.
	
	Page Factory is a way to initialize the web elements you want to interact with within the page
	object when you create an instance of it.
	