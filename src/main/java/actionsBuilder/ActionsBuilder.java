package actionsBuilder;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsBuilder {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/selectable.html");
		
		driver.manage().window().maximize();
		WebElement item1 = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement item2 = driver.findElementByXPath("//li[text()='Item 2']");
		WebElement item3 = driver.findElementByXPath("//li[text()='Item 3']");
		WebElement item4 = driver.findElementByXPath("//li[text()='Item 4']");
		WebElement item5 = driver.findElementByXPath("//li[text()='Item 5']");
		WebElement item6 = driver.findElementByXPath("//li[text()='Item 6']");
		WebElement item7 = driver.findElementByXPath("//li[text()='Item 7']");
		
		Actions builder=new Actions(driver);	
		//builder.moveToElement(item1).dragAndDrop(item1, item5).build().perform();
		builder.moveToElement(item1).
		sendKeys(Keys.CONTROL).
		click(item1).click(item3).click(item5).click(item7).perform();
	}

}
