package radioButton;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class RadioButton {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/radio.html");
		
		driver.manage().window().maximize();
		
		//driver.findElementByXPath("//input[@id='yes']").submit();
		//Selecing Radio Button
		driver.findElementByXPath("//input[@id='yes']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//input[@id='no']").click();
		
		//Find default Selected radio button
		List<WebElement> attribute = driver.findElementsByName("news");
		for (int i = 0; i < attribute.size(); i++) {
			boolean selected = attribute.get(i).isSelected();
			if (selected==true) {
				System.out.println("pass");
				
			}
		}
		
	
	}

}
