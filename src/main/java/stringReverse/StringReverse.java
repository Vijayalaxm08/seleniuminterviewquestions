package stringReverse;

public class StringReverse {

	public static void main(String[] args) {

		//Reverse a String or Number
		
		//Method 1:
		String text="I am doing good";
		char[] charArray = text.toCharArray();
		
		for (int i=charArray.length-1 ; i>=0; i--) {
			
			System.out.print(charArray[i]);
					
		}
		System.out.println(" ");
		
		//Method 2:
		String text1="I am doing good";
		for (int i = text1.length()-1; i >=0 ; i--) {
			System.out.print(text1.charAt(i));
		}
		
		System.out.println(" ");
		
		//Method 3:
		String text2="I am doing good";
		StringBuffer buffer=new StringBuffer(text2);
		StringBuffer reverse = buffer.reverse();
		System.out.print(reverse);
		
		System.out.println(" ");
		
		//Method 4:
		String text3="I am doing good";
		StringBuilder builder=new StringBuilder(text3);
		System.out.println(builder.reverse());
	}
}

/*Output:
	doog gniod ma I 
	doog gniod ma I 
	doog gniod ma I 
	doog gniod ma I
*/
	