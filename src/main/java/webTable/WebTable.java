package webTable;

import java.text.NumberFormat;
import java.text.ParseException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) throws ParseException {
		String max;
	     double m=0,r=0;
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://money.rediff.com/gainers/bse/daily/groupb");
		
		int cols = driver.findElementsByXPath("//table[@class='dataTable']//th").size();
		int row = driver.findElementsByXPath("//table[@class='dataTable']//tr").size();
		System.out.println(row);
		System.out.println(cols);
		
		for (int i = 1; i < row; i++) {
			
			max = driver.findElementByXPath("//table[@class='dataTable']//tr["+i+"]//td[3]").getText();
			 NumberFormat f =NumberFormat.getNumberInstance(); 
	            Number num = f.parse(max);
	            max = num.toString();
	            m = Double.parseDouble(max);
	            if(m>r)
	             {    
	                r=m;
	             }
			
		}
		System.out.println(r);
		
		String text = driver.findElementByXPath("//table[@class='dataTable']//tr[1]//td[3]").getText();
		System.out.println(text);
		
		
	}
	
}



