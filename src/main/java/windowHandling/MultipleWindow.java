package windowHandling;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class MultipleWindow {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Window.html");
		
		driver.manage().window().maximize();
		
		//Open Multiple Windows
		driver.findElementByXPath("//button[text()='Open Home Page']").click();
		driver.findElementByXPath("//button[text()='Open Multiple Windows']").click();
		driver.findElementByXPath("//button[text()='Do not close me ']").click();
		driver.findElementByXPath("//button[text()='Wait for 5 seconds']").click();
		Thread.sleep(5000);
		
		Set<String> allWin = driver.getWindowHandles();
		
		List<String> list=new ArrayList<String>();
		list.addAll(allWin);
		
		System.out.println("Total Window Opened is: "+list.size());
		for (int i = 1; i < list.size(); i++) {
			
			driver.switchTo().window(list.get(i));
			System.out.println(driver.getTitle());
			driver.close();
			driver.switchTo().window(list.get(0));
			
		}
		
		System.out.println(driver.getTitle());
		
	}

}
