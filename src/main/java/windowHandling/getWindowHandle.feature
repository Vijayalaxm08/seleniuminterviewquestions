
Handling Selenium Pop-up Window:

	When we have multiple windows in any web application, the activity may need to switch control
	among several windows from one to other in order to complete the operation. After completion
	of the operation, it has to return to the main window i.e. parent window.
	

Syntax:
	
	driver.getWindowHandle(); - RemoteWebDriver
	driver.getWindowHandles(); - RemoteWebDriver
	

		Set<String> allWin = driver.getWindowHandles();
		
		List<String> list=new ArrayList<String>();
		list.addAll(allWin);
		
		System.out.println("Total Window Opened is: "+list.size());
		for (int i = 1; i < list.size(); i++) {
			
			driver.switchTo().window(list.get(i));
			System.out.println(driver.getTitle());
			driver.close();
			driver.switchTo().window(list.get(0));
			
		}

Refer: 
	Indeed.java
	package week2.day4;
	