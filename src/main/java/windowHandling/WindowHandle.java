package windowHandling;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Window.html");
		
		driver.manage().window().maximize();
		
		// Open Home Page Button
		driver.findElementByXPath("//button[text()='Open Home Page']").click();
		
		Set<String> allWin = driver.getWindowHandles();
		
		//Converting to List since Set is unordered
		List<String> list=new ArrayList<String>();
		list.addAll(allWin);
		
		driver.switchTo().window(list.get(1));
		System.out.println(driver.getTitle());

		driver.close();
		
		driver.switchTo().window(list.get(0));
		System.out.println(driver.getTitle());
			
	}

}
