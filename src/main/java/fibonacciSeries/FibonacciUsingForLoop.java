package fibonacciSeries;

public class FibonacciUsingForLoop {

	public static void main(String[] args) {

		
		//http://www.java2novice.com/java-interview-programs/fibonacci-series/
		
		int febCount = 15;
        int[] feb = new int[febCount];
        feb[0] = 0;
        feb[1] = 1;
        for(int i=2; i < febCount; i++){
            feb[i] = feb[i-1] + feb[i-2];
        }

        for(int i=0; i< febCount; i++){
                System.out.print(feb[i] + " ");
        }
        
      //Method 2:
    	
    	// Set it to the number of elements you want in the Fibonacci Series
    			 int maxNumber = 10; 
    			 int previousNumber = 0;
    			 int nextNumber = 1;
    			 
    		   System.out.print("Fibonacci Series of "+maxNumber+" numbers:");
    	 
    		        for (int i = 1; i <= maxNumber; ++i)
    		        {
    		            System.out.print(previousNumber+" ");
    		            /* On each iteration, we are assigning second number
    		             * to the first number and assigning the sum of last two
    		             * numbers to the second number
    		             */
    	     		      
    		            int sum = previousNumber + nextNumber;
    		            previousNumber = nextNumber;
    		            nextNumber = sum;
    		        }
    		        
    	//Method 3: Using while loop
    		        
    		        int maxNumber1 = 10, previousNumber1 = 0, nextNumber1 = 1;
    		        System.out.print("Fibonacci Series of "+maxNumber1+" numbers:");
    	 
    		        int i=1;
    		        while(i <= maxNumber1)
    		        {
    		            System.out.print(previousNumber+" ");
    		            int sum1 = previousNumber1 + nextNumber1;
    		            previousNumber1 = nextNumber1;
    		            nextNumber1 = sum1;
    		            i++;
    		        }
    	 
    		}

	}
	
	
/*Output:
	
	0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 
	Fibonacci Series of 10 numbers:0 1 1 2 3 5 8 13 21 34 
	Fibonacci Series of 10 numbers:55 55 55 55 55 55 55 55 55 55 */

