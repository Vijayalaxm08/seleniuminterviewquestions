String Methods: Reference : (https://beginnersbook.com/2013/12/java-strings/)
							(https://www.javatpoint.com/java-string)
							(https://www.tutorialspoint.com/java/java_strings.htm)

1. charAt(index) - index starts from 0 to n-
	
	Ex: 1
	String name="javatpoint";  
	char ch=name.charAt(4);//returns the char value at the 4th index  
	System.out.println(ch); 
	
	Output:- "t"
	
	Ex: 2
	String name="javatpoint";  
	char ch=name.charAt(10);//returns the char value at the 10th index  
	System.out.println(ch); 
	
	Output:- StringIndexOutOfBoundsException
	
	Ex: 3
	String str = "Welcome to Javatpoint portal";      
    int strLength = str.length();      
    System.out.println("Character at 0 index is: "+ str.charAt(0));
    System.out.println("Character at last index is: "+ str.charAt(strLength-1));
    
    Output: - Character at 0 index is: W
			  Character at last index is: l

	Ex: 4
	String str = "Welcome to Javatpoint portal";          
    for (int i=0; i<=str.length()-1; i++) {  
         if(i%2!=0) {  
             System.out.println("Char at "+i+" place "+str.charAt(i));  
            }  
     }
     
     Output:- 
        Char at 1 place e
		Char at 3 place c
		Char at 5 place m
		Char at 7 place  
		Char at 9 place o
		Char at 11 place J
		Char at 13 place v
		Char at 15 place t
		Char at 17 place o
		Char at 19 place n
		Char at 21 place  
		Char at 23 place o
		Char at 25 place t
		Char at 27 place l
		
	Ex: 5
	String str = "Welcome to Javatpoint portal";  
    int count = 0;  
    for (int i=0; i<=str.length()-1; i++) {  
         if(str.charAt(i) == 't') {  
             count++;  
         }  
    }  
    System.out.println("Frequency of t is: "+count);
    
    Ouput:- Frequency of t is: 4
    
2. length() - It returns count of total number of characters

	Ex: 1
	String s1="javatpoint";  
	String s2="python";  
	System.out.println("string length is: "+s1.length());
	System.out.println("string length is: "+s2.length());
	
	Output:- string length is: 10
			 string length is: 6
	
	Ex: 2
	String str = "Javatpoint";  
    if(str.length()>0) {  
        System.out.println("String is not empty and length is: "+str.length());  
    }  
    str = "";  
    if(str.length()==0) {  
        System.out.println("String is empty now: "+str.length());  
    } 
 
3. format()
	
		String str1 = String.format("%d", 101);          // Integer value  
        String str2 = String.format("%s", "Amar Singh"); // String value  
        String str3 = String.format("%f", 101.00);       // Float value  
        String str4 = String.format("%x", 101);          // Hexadecimal value  
        String str5 = String.format("%c", 'c');          // Char value  
        System.out.println(str1);  
        System.out.println(str2);  
        System.out.println(str3);  
        System.out.println(str4);  
        System.out.println(str5);
	
	Output:
		101
		Amar Singh
		101.000000
		65
		c
4. substring(start, end) or substring(start index)

	Ex: 1
	
	String s1="javatpoint";  
	System.out.println(s1.substring(2,4));//returns va  
	System.out.println(s1.substring(2));//returns vatpoint  

5. contains() -returns boolean
	
	Ex: 1
	String name="what do you know about me";  
	System.out.println(name.contains("do you know"));  //returns true 
	System.out.println(name.contains("about"));  	//returns true
	System.out.println(name.contains("hello"));   //returns false
	
6. join()

	Ex: 1
	String joinString1=String.join("-","welcome","to","javatpoint");  
	System.out.println(joinString1); // output: welcome-to-javatpoint
	
	Ex: 2
		String date = String.join("/","25","06","2018");    
        System.out.print(date);    
        String time = String.join(":", "12","10","10");  
        System.out.println(" "+time);   Output: 25/06/2018 12:10:10

7. equals() - compares the two given strings based on the content of the string.

	Ex: 1
	String s1="javatpoint";  
	String s2="javatpoint";  
	String s3="JAVATPOINT";  
	String s4="python";  
	System.out.println(s1.equals(s2));//true because content and case is same  
	System.out.println(s1.equals(s3));//false because case is not same  
	System.out.println(s1.equals(s4));//false because content is not same  	

8. isEmpty()
	
	Ex: 1
	String s1="";  
	String s2="javatpoint";  
	  
	System.out.println(s1.isEmpty());  //true
	System.out.println(s2.isEmpty());  //false

9. concat() - method combines specified string at the end of this string

	Ex: 1
	String s1="java string";  
	s1.concat("is immutable");  
	System.out.println(s1);  
	s1=s1.concat(" is immutable so assign it explicitly");  
	System.out.println(s1);  
	
	Output:
	java string
	java string is immutable so assign it explicitly
	
	Ex: 2
		String str1 = "Hello";  
        String str2 = "Javatpoint";  
        String str3 = "Reader";  
       
        String str4 = str1.concat(" ").concat(str2).concat(" ").concat(str3);  
        System.out.println(str4);       //Hello Javatpoint Reader  
          
        String str5 = str1.concat("!!!");  
        System.out.println(str5);        //Hello!!! 
        String str6 = str1.concat("@").concat(str2);  
        System.out.println(str6);  //Hello@Javatpoint

10. replace()

	Ex: 1
	String s1="javatpoint is a very good website";  
	String replaceString=s1.replace('a','e');//replaces all occurrences of 'a' to 'e'  
	System.out.println(replaceString);   //jevetpoint is e very good website
	
	Ex: 2
	String s1="my name is khan my name is java";  
	String replaceString=s1.replace("is","was");//replaces all occurrences of "is" to "was"  
	System.out.println(replaceString);      //my name was khan my name was java
        
11. equalsIgnoreCase()
	
	Ex: 1
	String s1="javatpoint";  
	String s2="javatpoint";  
	String s3="JAVATPOINT";  
	String s4="python";  
	System.out.println(s1.equalsIgnoreCase(s2));//true because content and case both are same  
	System.out.println(s1.equalsIgnoreCase(s3));//true because case is ignored  
	System.out.println(s1.equalsIgnoreCase(s4));//false because content is not same       

12. split()

	Ex: 1
	String s1="java string split method by javatpoint";  
	String[] words=s1.split("\\s");//splits the string based on whitespace  
	 
	for(String w:words){  
	System.out.println(w);  
	}   
	
	Output: java
			string
			split
			method
			by
			javatpoint
    Ex: 2
    
    String s1="welcome to split world";  
	System.out.println("returning words:");  
	for(String w:s1.split("\\s",0)){  
	System.out.println(w);  
	}  
	Output: returning words:
			welcome 
			to 
			split 
			world
	System.out.println("returning words:");  
	for(String w:s1.split("\\s",1)){  
	System.out.println(w);  
	}  
	Output: returning words:
			welcome to split world
	
	System.out.println("returning words:");  
	for(String w:s1.split("\\s",2)){  
	System.out.println(w);  
	}  
	Output: returning words:
			welcome 
			to split world
	
	Ex: 3
	String str = "Javatpointtt";  
        System.out.println("Returning words:");  
        String[] arr = str.split("t", 0);  
        for (String w : arr) {  
            System.out.println(w);  
        }  
        System.out.println("Split array length: "+arr.length);  
    }  
    
    Output:  Returning words:
			Java
			poin
			Split array length: 2
    
13. indexOf()

	Ex: 1
	String s1="this is index of example";  
	int index1=s1.indexOf("is"); 
	int index2=s1.indexOf("index");
	System.out.println(index1+"  "+index2); //2 8  
	  
	//passing substring with from index  
	int index3=s1.indexOf("is",4);//returns the index of is substring after 4th index  
	System.out.println(index3);//5 
	  
	//passing char value  
	int index4=s1.indexOf('s');//returns the index of s char value  
	System.out.println(index4);//3  
	
	Ex: 2
		 String s1 = "This is indexOf method";         
        // Passing substring and index  
        int index = s1.indexOf("method", 10); //Returns the index of this substring  
        System.out.println("index of substring "+index);  
        index = s1.indexOf("method", 20); // It returns -1 if substring does not found  
        System.out.println("index of substring "+index);              
        
14. toLowerCase()

	Ex: 1
	String s1="JAVATPOINT HELLO stRIng";  
	String s1lower=s1.toLowerCase();  
	System.out.println(s1lower); 

15. toUpperCase()
	
	Ex: 1
	String s1="hello string";  
	String s1upper=s1.toUpperCase();  
	System.out.println(s1upper);     

16. trim()
	
	Ex: 1
	String s1="  hello string   ";  
	System.out.println(s1+"javatpoint");// hello string   javatpoint
	System.out.println(s1.trim()+"javatpoint");//hello stringjavatpoint
	
17. toCharArray()
18. toString()