String: Ref Page: (https://www.javatpoint.com/java-string)

	The CharSequence interface is used to represent the sequence of characters.
	String, StringBuffer and StringBuilder classes implement CharSequence.
	It means, we can create strings in java by using these three classes.

	CharSequence:
	/	|	\       - implements
String  S.B	 S.Builder	


	String is an object that represents a sequence of characters. The java.lang.String class is
	used to create a string object.

There are two ways to create String object:

	1. By string literal
	2. By new keyword